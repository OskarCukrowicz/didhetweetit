﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DidHeTweetIt
{
    class Program
    {
        static void Main(string[] args)
        {
            var sanders = DocumentVector.ReadVectorsFromTsv("C:\\Users\\Przemek\\source\\repos\\Did_He_Tweet_It\\DidHeTweetIt\\bernie.csv").GetRange(0,798);
            var trumpTrain = DocumentVector.ReadVectorsFromTsv("C:\\Users\\Przemek\\source\\repos\\Did_He_Tweet_It\\DidHeTweetIt\\trump_train.csv");
            var trumpTest = DocumentVector.ReadVectorsFromTsv("C:\\Users\\Przemek\\source\\repos\\Did_He_Tweet_It\\DidHeTweetIt\\trump_test.csv");

            Console.WriteLine("Trump:");
            CosineVerifyList(trumpTest, trumpTrain, (float)0.5);
            Console.WriteLine("Sanders:");
            CosineVerifyList(sanders, trumpTrain, (float)0.5);
        }

        public static void VerifyList(List<DocumentVector> test, List<DocumentVector> train)
        {
            VerifyList(test, train, (float)1.0);
        }

        public static void VerifyList(List<DocumentVector> test, List<DocumentVector> train, float treshold)
        {
            //Console.WriteLine(test.Count);
            int positiveCounter = 0;
            for (int i = 0; i < test.Count; ++i)
            {
                bool result = Verificator.Verify(7, test.ElementAt(i).Vector, train, treshold);
                if (result)
                {
                    ++positiveCounter;
                }
                //Console.WriteLine(i);
            }
            Console.WriteLine(positiveCounter + "/" + test.Count + " was positive.");
        }

        public static void CosineVerifyList(List<DocumentVector> test, List<DocumentVector> train)
        {
            CosineVerifyList(test, train, (float)1.0);
        }

        public static void CosineVerifyList(List<DocumentVector> test, List<DocumentVector> train, float treshold)
        {
            //Console.WriteLine(test.Count);
            int positiveCounter = 0;
            for (int i = 0; i < test.Count; ++i)
            {
                bool result = CosineVerificator.Verify(7, test.ElementAt(i).Vector, train, treshold);
                if (result)
                {
                    ++positiveCounter;
                }
                //Console.WriteLine(i);
            }
            Console.WriteLine(positiveCounter + "/" + test.Count + " was positive.");
        }


    }
}
