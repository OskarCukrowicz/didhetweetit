﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DidHeTweetIt
{
    public class Distance : IComparable
    {
        public long Id { get; set; }
        public float Value { get; }
        public bool ExpectedAuthor { get; set; }

        public Distance(long id, float value)
        {
            Id = id;
            Value = value;
        }

        public Distance(long id, float[] a, float[] b)
        {
            float diff, sum = 0;
            for (long i = 0; i < a.Length; ++i)
            {
                diff = a[i] - b[i];
                sum += diff * diff;
            }
            Value = (float)Math.Sqrt(sum);
            Id = id;
        }

        public Distance(long id, float[] a, float[] b, bool expectedAuthor)
        {
            float diff, sum = 0;
            for (long i = 0; i < a.Length; ++i)
            {
                diff = a[i] - b[i];
                sum += diff * diff;
            }
            Value = (float)Math.Sqrt(sum);
            Id = id;
            ExpectedAuthor = expectedAuthor;
        }

        public override string ToString()
        {
            return Id.ToString() + " " + Value.ToString();
        }

        public int CompareTo(object obj)
        {
            Distance that = obj as Distance;
            return Value.CompareTo(that.Value);
        }

        public static float Average(List<Distance> distances)
        {
            float result = (float)0.0;
            foreach (Distance distance in distances)
            {
                result += distance.Value;
            }
            return result / (float)distances.Count;
        }
    }

    public class CosineDistance : IComparable
    {
        public long Id { get; }
        public float Value { get; set; }
        public CosineDistance(long id, float value)
        {
            Id = id;
            Value = value;
        }

        public CosineDistance(long id, float[] a, float[] b)
        {
            float result = (float)0.0, aSum = (float)0.0, bSum = (float)0.0;
            for (long i = 0; i < a.Length; ++i)
            {
                result += a[i] * b[i];
                aSum += a[i] * a[i];
                bSum += b[i] * b[i];
            }
            Value = 1 - (result / ((float)Math.Sqrt(aSum) * (float)Math.Sqrt(bSum)));
            Id = id;
        }

        public int CompareTo(object obj)
        {
            CosineDistance that = obj as CosineDistance;
            return Value.CompareTo(that.Value);
        }


        public static float Average(List<CosineDistance> distances)
        {
            float result = (float)0.0;
            foreach (CosineDistance distance in distances)
            {
                result += distance.Value;
            }
            return result / (float)distances.Count;
        }
    }

    public class Verificator
    {
        public static bool Verify(int k, float[] vector, List<DocumentVector> documentVectors)
        {
            return Verify(k, vector, documentVectors, (float)1.0);
        }
        public static bool Verify(int k, float[] vector, List<DocumentVector> documentVectors, float treshold)
        {
            for (int i = 0; i < documentVectors.Count; ++i)
            {
                documentVectors.ElementAt(i).Id = i;
            }
            List<Distance> outerDisatances = GetDistances(vector, documentVectors);
            float[] vectorOfNearestNeighbour = documentVectors.First(item => item.Id == outerDisatances.ElementAt(0).Id).Vector;
            float averageDistance = Distance.Average(GetDistances(vectorOfNearestNeighbour, documentVectors.GetRange(0, k)));
            int result = 0;
            foreach (Distance distance in outerDisatances.GetRange(0, k))
            {
                if (distance.Value / averageDistance < treshold)
                {
                    ++result;
                }
            }
            if (result * 2 >= k)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static List<Distance> KNearestNeighbours(int k, float[] neighboured, List<DocumentVector> knownDocuments)
        {
            List<Distance> distances = GetDistances(neighboured, knownDocuments);
            return distances.GetRange(0, k);
        }

        public static List<Distance> GetDistances(float[] poinOfReference, List<DocumentVector> vectors)
        {
            List<Distance> distances = new List<Distance>();
            foreach (DocumentVector document in vectors)
            {
                distances.Add(new Distance(document.Id, poinOfReference, document.Vector));
            }
            distances.Sort();
            return distances;
        }

        public static List<DocumentVector> DocumentVectorsOfAllAuthors(string expectedAuthor, List<DocumentVectorsOfSingleAuthor> vectorsOfSingleAuthors)
        {
            List<DocumentVector> result = new List<DocumentVector>();
            foreach (DocumentVectorsOfSingleAuthor e in vectorsOfSingleAuthors)
            {
                if (e.Author.Equals(expectedAuthor))
                {
                    foreach (DocumentVector e2 in e.Vectors)
                    {
                        e2.ExpectedAuthor = true;
                        result.Add(e2);
                    }
                }
                else
                {
                    foreach (DocumentVector e2 in e.Vectors)
                    {
                        e2.ExpectedAuthor = false;
                        result.Add(e2);
                    }
                }
            }
            return result;
        }

        public static float AverageAverageDistance(List<DocumentVector> documentVectors)
        {
            float averageAverage = (float)0.0;
            for (int i = 0; i < documentVectors.Count; ++i)
            {
                Console.WriteLine(i);
                List<Distance> distances = new List<Distance>();
                foreach (DocumentVector document in documentVectors)
                {
                    distances.Add(new Distance(document.Id, documentVectors.ElementAt(i).Vector, document.Vector));
                }
                averageAverage += Distance.Average(distances);
            }
            return averageAverage / (float)documentVectors.Count;
        }
    }

    public class CosineVerificator
    {
        public static bool Verify(int k, float[] vector, List<DocumentVector> documentVectors)
        {
            return Verify(k, vector, documentVectors, (float)1.0);
        }
        public static bool Verify(int k, float[] vector, List<DocumentVector> documentVectors, float treshold)
        {
            for (int i = 0; i < documentVectors.Count; ++i)
            {
                documentVectors.ElementAt(i).Id = i;
            }
            List<CosineDistance> outerDisatances = GetDistances(vector, documentVectors);
            float[] vectorOfNearestNeighbour = documentVectors.First(item => item.Id == outerDisatances.ElementAt(0).Id).Vector;
            float averageDistance = CosineDistance.Average(GetDistances(vectorOfNearestNeighbour, documentVectors.GetRange(0, k)));
            int result = 0;
            foreach (CosineDistance distance in outerDisatances.GetRange(0, k))
            {
                if (distance.Value / averageDistance < treshold)
                {
                    ++result;
                }
            }
            if (result * 2 >= k)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static List<CosineDistance> KNearestNeighbours(int k, float[] neighboured, List<DocumentVector> knownDocuments)
        {
            List<CosineDistance> distances = GetDistances(neighboured, knownDocuments);
            return distances.GetRange(0, k);
        }

        public static List<CosineDistance> GetDistances(float[] poinOfReference, List<DocumentVector> vectors)
        {
            List<CosineDistance> distances = new List<CosineDistance>();
            foreach (DocumentVector document in vectors)
            {
                distances.Add(new CosineDistance(document.Id, poinOfReference, document.Vector));
            }
            distances.Sort();
            return distances;
        }

        public static List<DocumentVector> DocumentVectorsOfAllAuthors(string expectedAuthor, List<DocumentVectorsOfSingleAuthor> vectorsOfSingleAuthors)
        {
            List<DocumentVector> result = new List<DocumentVector>();
            foreach (DocumentVectorsOfSingleAuthor e in vectorsOfSingleAuthors)
            {
                if (e.Author.Equals(expectedAuthor))
                {
                    foreach (DocumentVector e2 in e.Vectors)
                    {
                        e2.ExpectedAuthor = true;
                        result.Add(e2);
                    }
                }
                else
                {
                    foreach (DocumentVector e2 in e.Vectors)
                    {
                        e2.ExpectedAuthor = false;
                        result.Add(e2);
                    }
                }
            }
            return result;
        }

        public static float AverageAverageDistance(List<DocumentVector> documentVectors)
        {
            float averageAverage = (float)0.0;
            for (int i = 0; i < documentVectors.Count; ++i)
            {
                Console.WriteLine(i);
                List<CosineDistance> distances = new List<CosineDistance>();
                foreach (DocumentVector document in documentVectors)
                {
                    distances.Add(new CosineDistance(document.Id, documentVectors.ElementAt(i).Vector, document.Vector));
                }
                averageAverage += CosineDistance.Average(distances);
            }
            return averageAverage / (float)documentVectors.Count;
        }
    }
}
